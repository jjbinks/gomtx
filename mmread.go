package gomtx

import (
    "bufio"
    "os"
    "fmt"
    "log"
)

func eofpanic(nnz uint64, nl uint64) {
    panic(fmt.Sprintf(`Premature end-of-file.
Check that the data file contains %d lines of i,j,[val] data.
(it appears there are only %d such lines.)`, nnz, nl))
}

func MMread(filename string, nnzmax uint64) (
        rep string, field string, symm string,
        rows uint64, cols uint64, nnz uint64,
        indx []uint64, jndx []uint64,
        ival []int, rval []float64, cval []complex128) {

    file, err := os.Open(filename)
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)

    rep, field, symm, rows, cols, nnz = mmParseInfo(scanner)

    scanner.Split(bufio.ScanLines)

    if rep == "coordinate" {
        if nnz > nnzmax {
            panic(fmt.Sprintf(`insufficent array lengths for matrix of %d nonzeros.
resize nnzmax to at least %d. (currently %d)`, nnz, nnz, nnzmax))
        }
        indx = make([]uint64, nnz)
        jndx = make([]uint64, nnz)
        if field == "integer" {
            ival = make([]int, nnz)
            rval = make([]float64, 0)
            cval = make([]complex128, 0)
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(),
                        &indx[i], &jndx[i], &ival[i])
                if n == 0 || err != nil {
                    eofpanic(nnz, i+1)
                }
            }
        } else if field == "real" {
            ival = make([]int, 0)
            rval = make([]float64, nnz)
            cval = make([]complex128, 0)
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(),
                        &indx[i], &jndx[i], &rval[i])
                if n == 0 || err != nil {
                    eofpanic(nnz, i+1)
                }
            }
        } else if field == "complex" {
            ival = make([]int, 0)
            rval = make([]float64, 0)
            cval = make([]complex128, nnz)
            var cval_r, cval_i float64
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(),
                        &indx[i], &jndx[i],
                        &cval_r, &cval_i)
                if n == 0 || err != nil {
                    eofpanic(nnz, i+1)
                }
                cval[i] = complex(cval_r, cval_i)
            }
        } else { // field == "pattern"
            ival = make([]int, 0)
            rval = make([]float64, 0)
            cval = make([]complex128, 0)
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(),
                        &indx[i], &jndx[i])
                if n == 0 || err != nil {
                    eofpanic(nnz, i+1)
                }
            }
        }
    } else { // rep == "array"
        if nnz > nnzmax {
            panic(fmt.Sprintf(`insufficent array length for %d by %d dense %s matrix.
resize nnzmax to at least %d (currently %d)`, rows, cols, symm, nnz, nnzmax))
        }
        indx = make([]uint64, 0)
        jndx = make([]uint64, 0)
        if field == "integer" {
            ival = make([]int, nnz)
            rval = make([]float64, 0)
            cval = make([]complex128, 0)
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(), &ival[i])
                if n == 0 || err != nil {
                    eofpanic(nnz, i+1)
                }
            }
        } else if field == "real" {
            ival = make([]int, 0)
            rval = make([]float64, nnz)
            cval = make([]complex128, 0)
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(), &rval[i])
                if n == 0 || err != nil {
                    panic(err)
                    eofpanic(nnz, i+1)
                }
            }
        } else { // field == "complex"
            ival = make([]int, 0)
            rval = make([]float64, 0)
            cval = make([]complex128, nnz)
            var cval_r, cval_i float64
            for i := uint64(0); i < nnz; i++ {
                scanner.Scan()
                n, err := fmt.Sscanln(scanner.Text(), &cval_r, &cval_i)
                if n == 0 || err != nil {
                    eofpanic(nnz, i+1)
                }
                cval[i] = complex(cval_r, cval_i)
            }
        }
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return
}

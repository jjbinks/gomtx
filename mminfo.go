package gomtx

import (
    "bufio"
    "os"
    "log"
)

func MMinfo(filename string) (
        rep string, field string, symm string,
        rows uint64, cols uint64, entries uint64) {
    file, err := os.Open(filename)
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)

    rep, field, symm, rows, cols, entries = mmParseInfo(scanner)

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return
}


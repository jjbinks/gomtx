package gomtx

import (
    "bufio"
    "fmt"
    "strings"
    "strconv"
)

func mmParseInfo(scanner *bufio.Scanner) (
        rep string, field string, symm string,
        rows uint64, cols uint64, entries uint64) {

    scanner.Split(bufio.ScanLines)
    scanner.Scan() // read first line
    header := scanner.Text()
    headerwords := strings.Split(header, " ") // get list of words

    if len(headerwords) != 5 ||
       headerwords[0] != "%%MatrixMarket" {
        panic(fmt.Sprintf(`Invalid matrix header: %s
Correct header format:
%%%%MatrixMarket type representation field symmetry`, header))
    }

    rep   = headerwords[2]
    field = headerwords[3]
    symm  = headerwords[4]

    if headerwords[1] != "matrix" {
        panic(fmt.Sprintf(`Invalid matrix type: %s
This reader only understand type 'matrix'`, headerwords[1]))
    }

    // checking options respect mtx standard
    if rep != "coordinate" &&
       rep != "array" {
        panic(fmt.Sprintf(`'%s' representation is not recognized.
Recognized representations:
    coordinate
    array`, rep))
    }
    if rep == "coordinate" &&
       field != "integer" &&
       field != "real" &&
       field != "complex" &&
       field != "pattern" {
        panic(fmt.Sprintf(`'%s' field is not recognized.
Recognized fields:
    real
    complex
    integer
    pattern`, field))
    }
    if rep == "array" &&
       field != "integer" &&
       field != "real" &&
       field != "complex" {
        panic(fmt.Sprintf(`'%s' field is not recognized.
Recognized fields:
    real
    complex
    integer`, field))
    }
    if symm != "general" &&
       symm != "symmetric" &&
       symm != "hermitian" &&
       symm != "skew-symmetric" {
        panic(fmt.Sprintf(`'%s' symmetry is not recognized.
Recognized symmetries:
    general
    symmetric
    hermitian
    skew-symmetric`, symm))
    }

    // read lines until non-comment is found
    // (loop on "%..." or "")
    var line string
    for scanner.Scan() {
        line = scanner.Text()
        if len(line) > 0 &&
           line[0] != '%' {
            break
        }
    }
    sizeinfo := strings.Split(line, " ")

    if rep == "coordinate" {

        if len(sizeinfo) != 3 {
            panic(fmt.Sprintf(`Size info inconsistant with representation.
Coordinate matrices need exactly 3 size descriptors.
%v were found.`, len(sizeinfo)))
        }

        var err_rows, err_cols, err_entries error
        rows,    err_rows    = strconv.ParseUint(sizeinfo[0], 10, 32)
        cols,    err_cols    = strconv.ParseUint(sizeinfo[1], 10, 32)
        entries, err_entries = strconv.ParseUint(sizeinfo[2], 10, 64)
        if err_rows != nil {
            panic(fmt.Sprintf(`Error while converting rows value '%s'.`,
                              sizeinfo[0]))
        }
        if err_cols != nil {
            panic(fmt.Sprintf(`Error while converting cols value '%s'.`,
                              sizeinfo[1]))
        }
        if err_entries != nil {
            panic(fmt.Sprintf(`Error while converting entries value '%s'.`,
                              sizeinfo[2]))
        }

    } else { // rep == "array"

        if len(sizeinfo) != 2 {
            panic(fmt.Sprintf(`Size info inconsistant with representation.
Array matrices need exactly 2 size descriptors.
%v were found.`, len(sizeinfo)))
        }

        var err_rows, err_cols error
        rows, err_rows = strconv.ParseUint(sizeinfo[0], 10, 32)
        cols, err_cols = strconv.ParseUint(sizeinfo[1], 10, 32)
        if err_rows != nil {
            panic(fmt.Sprintf(`Error while converting rows value '%s'.`,
                              sizeinfo[0]))
        }
        if err_cols != nil {
            panic(fmt.Sprintf(`Error while converting cols value '%s'.`,
                              sizeinfo[1]))
        }

        if symm == "symmetric" ||
           symm == "hermitian" {
            entries = (rows*cols - rows)/2 + rows
        } else if symm == "skew-symmetric" {
            entries = (rows*cols - rows)/2
        } else {
            entries = rows*cols
        }
    }

    return
}


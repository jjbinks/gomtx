package gomtx

import (
    "testing"
    "fmt"
)

func getTestFilename(name string) (filename string) {
    filename = fmt.Sprintf("testfiles/%s.mtx", name);
    return
}

func TestMMinfo_header_good_dense(t *testing.T) {
    rep, field, symm, rows, cols, entries :=
        MMinfo(getTestFilename("good_header_1"))
    if rep  != "array"     || field   != "real" ||
       symm != "symmetric" || rows    != 2      ||
       cols != 2           || entries != 3 {
        t.Error("incorrect parse")
    }
}

func TestMMinfo_header_good_sparse(t *testing.T) {
    rep, field, symm, rows, cols, entries :=
        MMinfo(getTestFilename("good_header_2"))
    if rep  != "coordinate" || field   != "real" ||
       symm != "general"    || rows    != 3      ||
       cols != 3            || entries != 4 {
        t.Error("incorrect parse")
    }
}

func TestMMinfo_header_bad(t *testing.T) {
    bad_header_filenames := []string{
        "bad_header_1",
        "bad_header_2",
        "bad_header_3",
        "bad_header_4",
        "bad_header_5",
        "bad_header_6",
        "bad_header_7",
        "bad_header_8"}
    for _, bad_header_filename := range bad_header_filenames {
        func() {
            defer func() {
                if r := recover(); r == "noerror" {
                    t.Error("missed error in header")
                }
            }()
            MMinfo(getTestFilename(bad_header_filename))
            panic("noerror")
        }()
    }
}


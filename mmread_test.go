package gomtx

import (
    "fmt"
)

func ExampleMMread_1() {
    rep, field, symm, rows, cols, nnz,
    indx, jndx, ival, rval, cval :=
        MMread(getTestFilename("good_header_1"), 200)
    fmt.Printf(
`rep:%s field:%s symm:%s
rows:%d cols:%d nnz:%d
indx:%v jndx:%v
ival:%v rval:%v cval:%v
`,
               rep, field, symm, rows, cols, nnz,
               indx, jndx, ival, rval, cval)
    // Output: rep:array field:real symm:symmetric
    // rows:2 cols:2 nnz:3
    // indx:[] jndx:[]
    // ival:[] rval:[4.5 8.4 1.2] cval:[]
}

func ExampleMMread_2() {
    rep, field, symm, rows, cols, nnz,
    indx, jndx, ival, rval, cval :=
        MMread(getTestFilename("good_header_2"), 200)
    fmt.Printf(
`rep:%s field:%s symm:%s
rows:%d cols:%d nnz:%d
indx:%v jndx:%v
ival:%v rval:%v cval:%v
`,
               rep, field, symm, rows, cols, nnz,
               indx, jndx, ival, rval, cval)
    // Output: rep:coordinate field:real symm:general
    // rows:3 cols:3 nnz:4
    // indx:[1 2 2 3] jndx:[2 2 3 1]
    // ival:[] rval:[4.5 8.4 1.2 8.5] cval:[]
}

